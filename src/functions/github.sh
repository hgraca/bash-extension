#!/usr/bin/env bash

#
# Gets the latest tag of a github repository
# Synopsis: github.ge_latest_tag <user> <repo>
# Ex: github.ge_latest_tag docker compose
#
github.ge_latest_tag() {
  USER="$1"
  REPO="$2"

  curl -s https://api.github.com/repos/"${USER}"/"${REPO}"/releases/latest | sed -Ene '/^ *"tag_name": *"(v.+)",$/s//\1/p'
}
