#!/usr/bin/env bash
# Bash 5.1

#
# Waits for a list of parallel threads to finish echoes its exit code and returns non-zero is any of them returns non-zero
# NOTE: It only works from Bash 5.1
# Ex:
#   declare -A PID_LIST
#   commandA 1 > a.out 2>&1 &
#   PID_LIST[$!]='Thread A'
#   commandB 2 > b.out 2>&1 &
#   PID_LIST[$!]='Thread B'
#   commandB 3 > c.out 2>&1 &
#   PID_LIST[$!]='Thread C'
#   declare -A RESULT=()
#   parallel.wait_and_check_success_map PID_LIST RESULT
#   echo "Exit code: $?"
#
parallel.wait_and_check_success_map() {
  local -A NULL_OUTPUT=()
  local -n PID_WAIT_LIST="${1}"
  local -n OUTPUT="${2:-NULL_OUTPUT}"

  ERROR_COUNT=0
  while [ ${#PID_WAIT_LIST[@]} -ne 0 ]; do
    wait -n -p PID "${!PID_WAIT_LIST[@]}" # `-p` only from bash 5.1
    CODE=$?
    if [ "${CODE}" -ne "0" ]; then
      stdout.log_error "Command ${PID_WAIT_LIST[$PID]} failed with exit code $CODE."
      ERROR_COUNT=$((ERROR_COUNT + 1))
      OUTPUT[$PID]="${CODE}"
      unset "PID_WAIT_LIST[$PID]"
    else
      OUTPUT[$PID]="0"
      stdout.log_success "Command ${PID_WAIT_LIST[$PID]} successful"
    fi

    unset "PID_WAIT_LIST[$PID]"
  done
  return "${ERROR_COUNT}"
}

#
# Ex:
#   declare -A TASK_LIST=( \
#     [command_1_name]=command_1 \
#     [command_2_name]=command_2 \
#     [command_3_name]=command_3 \
#   )
#   parallel.cmd_map TASK_LIST
#
parallel.cmd_map() {
  local -n COMMAND_LIST="$1"
  local -A PID_LIST=()
  OUT_PATH='/tmp'

  COUNT=$(array.count COMMAND_LIST)
  LOG_LIST=''
  stdout.log_info ""
  stdout.log_info "Will run ${COUNT} commands in parallel:"
  for COMMAND_NAME in "${!COMMAND_LIST[@]}"; do
    stdout.log_info "    ${COMMAND_NAME}: '${COMMAND_LIST[${COMMAND_NAME}]}' (tail -n 9999 -f ${OUT_PATH}/${COMMAND_NAME}.out)"
    LOG_LIST="${LOG_LIST}\n -f ${OUT_PATH}/${COMMAND_NAME}.out \ "
  done
  LOG_LIST="$(string.rightTrim '\ ' "${LOG_LIST}")"
  stdout.log_info ""
  stdout.log_info "Tail all logs with:"
  stdout.log_info "tail -n 9999 \ ${LOG_LIST}"

  stdout.log_info ""
  stdout.log_info "Running..."
  ORIGINAL_PROCESSES=()
  for COMMAND_NAME in "${!COMMAND_LIST[@]}"; do
    COMMAND="${COMMAND_LIST[${COMMAND_NAME}]}"
    stdout.log_debug "eval \"${COMMAND}\" > \"${OUT_PATH}/${COMMAND_NAME}.out\" 2>&1 &"
    eval "time ${COMMAND}" > "/${OUT_PATH}/${COMMAND_NAME}.out" 2>&1 &
    PID_LIST[$!]="${COMMAND_NAME}"
    ORIGINAL_PROCESSES[$!]="${COMMAND_NAME}"
  done

  declare -A PARALLEL_RESULTS=()
  parallel.wait_and_check_success_map PID_LIST PARALLEL_RESULTS

  RESULT_EXIT_CODE=0

  # print the output of ONLY the failed commands
  for PID in "${!PARALLEL_RESULTS[@]}"; do
    RESULT="${PARALLEL_RESULTS[${PID}]}"
    if [ "${RESULT}" -ne "0" ]; then
      ((RESULT_EXIT_CODE++))
      COMMAND_NAME="${ORIGINAL_PROCESSES[${PID}]}"
      echo ""
      echo ""
      echo ""
      stdout.log_error "==============================================================================================="
      stdout.log_error "Output of command ${COMMAND_NAME}:"
      stdout.log_error "$(cat "${OUT_PATH}/${COMMAND_NAME}.out")"
    fi
  done

  return $RESULT_EXIT_CODE
}
