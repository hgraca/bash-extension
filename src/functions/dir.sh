#!/usr/bin/env bash

dir.count_dir() {
  PATH_TO_COUNT="$1"

  ls -da ${PATH_TO_COUNT}/*/ | wc -l
}

dir.exists() {
  FILE_PATH="$1"

  if [ -e "${FILE_PATH}" ] && [ -d "${FILE_PATH}" ]; then
    return 0
  fi

  return 1
}
