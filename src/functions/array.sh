#!/usr/bin/env bash

# Ex:
#  IN=("something to search for" "a string" "test2000")
#  array.contains "a string" IN
#  echo $?
#  0
#  array.contains "blaha" IN
#  echo $?
#  1
array.contains() {
  local NEEDLE="$1"
  declare -n HAYSTACK="$2"

  for i in "${!HAYSTACK[@]}"; do
    if [[ "${HAYSTACK[$i]}" = "${NEEDLE}" ]]; then
      return 0
    fi
  done

  return 1
}


# Ex:
#  IN=("something to search for" "a string" "test2000")
#  INDEX=$(array.find_index "a string" IN)
array.find_index () {
  local NEEDLE="$1"
  declare -n HAYSTACK="$2"

  for i in "${!HAYSTACK[@]}"; do
    if [[ "${HAYSTACK[$i]}" = "${NEEDLE}" ]]; then
      INDEX=$i
      break
    fi
  done

  if [ ${INDEX} -gt -1 ]; then
    echo "$(($INDEX))"
  else
    return 1
  fi
}

# Ex:
#  IN=("something to search for" "a string" "test2000")
#  array.reverse IN OUT
#  echo ${OUT[@]}
array.reverse() {
  declare -n arr="$1" rev="$2"
  for i in "${arr[@]}"; do
    rev=("$i" "${rev[@]}")
  done
}

# Ex:
#  IN=("something to search for" "a string" "test2000")
#  COUNT=$(array.count IN)
array.count() {
  declare -n ARRAY="$1"

  echo ${#ARRAY[@]}
}

# Returns the list of elements, from either array, which are unique
# Ex:
#  array.diff IN1 IN2
array.diff() {
  declare -n ARRAY1="$1"
  declare -n ARRAY2="$2"

  echo "${ARRAY1[@]}" "${ARRAY2[@]}" | tr ' ' '\n' | sort | uniq -u
}

#
# Ex:
#   IN=("something to search for" "a string" "test2000")
#   if array.is_empty IN; then
#     ...
#   fi
#
array.is_empty() {
  declare -n ARRAY="$1"

  COUNT=$(array.count ARRAY)

  if [ "${COUNT}" -gt 0 ]; then
    return 1
  fi

  return 0
}

array.max() {
  declare -n ARRAY="$1"

  TMP_IFS="${IFS}"
  IFS=$'\n'

  echo "${ARRAY[*]}" | sort -nr | head -n1

  IFS="${TMP_IFS}"
}

array.print() {
  local -n ARRAY="$1"

  for key in "${!ARRAY[@]}"; do
    echo "$key => ${ARRAY[$key]}";
  done
}
