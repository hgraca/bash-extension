#!/usr/bin/env bash

math.random_int() {
  local FROM=0 TO=9999999
  local "${@}"

  shuf -i ${FROM}-${TO} -n1
}
