#!/usr/bin/env bash

TESTS_PATH=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

BASH_OVERLAY_LOG_LEVEL=${BASH_OVERLAY_LOG_LEVEL_INFO}

CUSTOM_BOOTSTRAP_PATH="${TESTS_PATH}/bootstrap.custom.sh"
if [ -e "${CUSTOM_BOOTSTRAP_PATH}" ] && [ ! -d "${CUSTOM_BOOTSTRAP_PATH}" ]; then
    . "${CUSTOM_BOOTSTRAP_PATH}"
fi
