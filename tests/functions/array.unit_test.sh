#!/usr/bin/env bash

unit_test.array.print() {
  local -A MAP=( \
      [HDD]=Samsung \
      [Monitor]=Dell \
      [Keyboard]=A4Tech \
  )
  OUT=$(array.print MAP)
  EXPECTED_OUT='Keyboard => A4Tech
Monitor => Dell
HDD => Samsung'

  if [ "${OUT}" == "${EXPECTED_OUT}" ]; then
    return 0
  fi

  echo "Printed '${OUT}'"
  echo "but expected '${EXPECTED_OUT}'."
  return 1
}
