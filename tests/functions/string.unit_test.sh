#!/usr/bin/env bash

unit_test.it_should_validate_that_haystack_ends_with() {
  NEEDLE='/'
  HAYSTACK='something/something'

  if string.ends_with "${NEEDLE}" "${HAYSTACK}"; then
    return 1
  fi

  return 0
}
