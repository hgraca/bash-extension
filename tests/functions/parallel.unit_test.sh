#!/usr/bin/env bash

unit_test.it_should_run_two_commands_in_parallel() {
  COUNT=1
  MAX_TRIES=3
  it_should_run_two_commands_in_parallel
  RETURN_CODE=$?
  while [ ${RETURN_CODE} -ne 0 ] && [ ${COUNT} -lt ${MAX_TRIES} ]; do
    it_should_run_two_commands_in_parallel
    RETURN_CODE=$?
    COUNT+=1
  done

  if [ ${RETURN_CODE} -ne 0 ]; then
    stdout.log_error "The test should take between 1 and 2 seconds. It took: ${DURATION}s"
    stdout.log_error "Sometimes it can happen, by coincidence, that it takes exactly 2s, so we tried the test ${MAX_TRIES} times."
    return 1
  fi

  return 0
}

it_should_run_two_commands_in_parallel() {
  local -A TASK_LIST=( \
    [command_1]="sleep 1" \
    [command_2]="sleep 1" \
  )
  START=$(date +%s)
  parallel.cmd_map TASK_LIST
  END=$(date +%s)
  DURATION=$((END-START))

  if [ ${DURATION} -lt 1 ] || [ ${DURATION} -ge 2 ]; then
    return 1
  fi

  return 0
}
