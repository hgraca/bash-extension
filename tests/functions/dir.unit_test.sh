#!/usr/bin/env bash

unit_test.it_should_know_if_dir_exists() {
  TEST_TMP_DIR="${1}"

  DIR="${TEST_TMP_DIR}/test"
  mkdir -p "${DIR}"

  if dir.exists "${DIR}"; then
    return 0
  fi

  return 1
}

unit_test.it_should_know_if_dir_doesnt_exist() {
  TEST_TMP_DIR="${1}"

  DIR="${TEST_TMP_DIR}/test"

  if dir.exists "${DIR}"; then
    return 1
  fi

  return 0
}
