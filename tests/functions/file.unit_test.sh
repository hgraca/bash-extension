#!/usr/bin/env bash

unit_test.it_should_get_modification_time() {
  TEST_TMP_DIR="${1}"

  FILE1="${TEST_TMP_DIR}/test1.txt"
  touch "${FILE1}"

  if [ "$(date -r "${FILE1}" '+%Y%m%d%H%M%S')" == "$(file.get_modification_time "${FILE1}")" ]; then
    return 0
  fi

  return 1
}

unit_test.it_should_know_if_file_is_newer_or_not() {
  TEST_TMP_DIR="${1}"

  FILE1="${TEST_TMP_DIR}/test1.txt"
  touch "${FILE1}"
  sleep 1
  FILE2="${TEST_TMP_DIR}/test2.txt"
  touch "${FILE2}"

  if ! file.file1_is_newer_than_file2 "${FILE2}" "${FILE1}"; then
    stdout.log_error "It thinks ${FILE2} is NOT newer than ${FILE1}"
    return 1
  fi

  if file.file1_is_newer_than_file2 "${FILE1}" "${FILE2}"; then
    stdout.log_error "It thinks ${FILE1} is newer than ${FILE2}"
    return 2
  fi

  if file.file1_is_newer_than_file2 "${FILE1}" "${FILE1}"; then
    stdout.log_error "It thinks ${FILE1} is newer than ${FILE1}"
    return 3
  fi

  return 0
}

unit_test.it_should_know_if_files_have_same_modification_time() {
  TEST_TMP_DIR="${1}"

  FILE1="${TEST_TMP_DIR}/test1.txt"
  touch "${FILE1}"
  sleep 1
  FILE2="${TEST_TMP_DIR}/test2.txt"
  touch "${FILE2}"

  if file.have_same_modification_time "${FILE1}" "${FILE1}" && ! file.have_same_modification_time "${FILE1}" "${FILE2}"; then
    return 0
  fi

  return 1
}
