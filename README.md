# Bash Extension

A library of domain agnostic Bash functions, with clear names, which can be reused across projects as if they
were part of Bash itself.

Hopefully, this will make it simpler to create new Bash scripts, and it will make them more readable.

## How to use

You can install this library in your project, using [composer](https://getcomposer.org/).

Once you have composer installed in your system, you can run `composer install`,
which will install all dependencies and generate the autoloader in `vendor/autoload.sh`.

Include the file `vendor/autoload.sh` in your script, and you are good to go.

Ex:

```bash
#!/usr/bin/env bash

. "./vendor/autoload.sh" # Make sure the path is correct

if os.is_ubuntu; then
  echo "Is Ubuntu!"
elif os.is_alpine; then
  echo "Is Alpine!"
else
  echo "Unknown distribution"
fi
```

### Running the tests

You can run the tests by running `composer test`.
